<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Event;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $event = new Event();
        $event->title = "Návštěva doktora";
        $event->note = "";
        $event->start_date = "2023-11-23";
        $event->save();

        $event = new Event();
        $event->title = "Chata";
        $event->note = "";
        $event->start_date = "2023-11-24";
        $event->end_date = "2023-11-26";
        $event->save();

        $event = new Event();
        $event->title = "Výlet na hory";
        $event->note = "";
        $event->start_date = "2023-10-20";
        $event->end_date = "2023-10-22";
        $event->save();

        $event = new Event();
        $event->title = "Nákup";
        $event->note = "";
        $event->start_date = "2023-11-23";
        $event->save();
    }
}
