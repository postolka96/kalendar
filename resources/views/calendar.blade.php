<!DOCTYPE html>
<html lang="cs">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Kalendář') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    {{-- Fontawesome --}}
    <script src="https://kit.fontawesome.com/27cb10a11c.js"></script>
</head>

<body class="bg-dark">
    <div id="app" class="">
        <calendar-component></calendar-component>
    </div>
</body>

</html>
