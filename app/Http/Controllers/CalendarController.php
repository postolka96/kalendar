<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Event;
use Carbon\CarbonPeriod;

class CalendarController extends Controller
{
    public function getMonth()
    {
        $date = Carbon::now();

        $weeks = $this->findDays($date);
        $actualMY = Carbon::now();

        return ['actualMY' => $actualMY, 'weeks' => $weeks];
    }

    public function changeMonth(Request $request)
    {
        $actualMY = Carbon::Parse($request['actualMY']);

        if ($request['move']) {
            $date = $actualMY->day()->addMonth();
        } else {
            $date = $actualMY->day();
        }

        $weeks = $this->findDays($date);
        $actualMY = $date;

        return ['actualMY' => $actualMY, 'weeks' => $weeks];
    }

    public function getEvents(Request $request)
    {
        $date = Carbon::Parse($request['actualMY']);

        $start = $date->copy()->firstOfMonth()->startOfWeek(Carbon::MONDAY);
        $end = $date->lastOfMonth()->endOfWeek(Carbon::SUNDAY);
        $events = Event::whereBetween('start_date', [$start, $end])->orderBy('start_date')->get();

        return $events;
    }

    public function addEvent(Request $request) {

         $event = new Event();
         $event->title = $request->event['title'];
         $event->start_date = $request->event['from'];
         if($request->event['to']) {
            $event->end_date = $request->event['to'];
         }
         if($request->event['note']) {
            $event->note = $request->event['note'];
         }
         $event->save();

         return ['success'=>true];
    }

    private function findDays($date)
    {
        $start = $date->copy()->firstOfMonth()->startOfWeek(Carbon::MONDAY);
        $end = $date->copy()->lastOfMonth()->endOfWeek(Carbon::SUNDAY);

        $eventsOneDay = Event::whereBetween('start_date', [$start, $end])->whereNull('end_date')->pluck('start_date')->toArray();
        $eventsMoreDaysFirstsDay = Event::whereBetween('start_date', [$start, $end])->whereNotNull('end_date')->get();

        $eventsMoreDays = [];
        foreach ($eventsMoreDaysFirstsDay as $ed) {
            $eventAllDays = CarbonPeriod::create(Carbon::Parse($ed->start_date), Carbon::Parse($ed->end_date));
            foreach ($eventAllDays as $ead) {
                $eventsMoreDays[] = $ead;
            }
        }

        $megreArray = array_merge($eventsOneDay, $eventsMoreDays);
        $events = array_unique($megreArray);

        $weeks = [];

        while ($start <= $end) {
            $days = [];
            $d = 1;

            while ($d <= 7) {
                $newDay = new Carbon($start);
                if (in_array($newDay, $events)) {
                    $day = ['date' => $newDay, 'hasEvent' => true];
                } else {
                    $day = ['date' => $newDay, 'hasEvent' => false];
                }
                $days[] = $day;
                $start->addDay();
                $d++;
            }

            $weeks[] = $days;
        }

        return $weeks;
    }
}
